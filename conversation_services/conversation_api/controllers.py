########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for

# Import models
from .models import Communication
from .models import CommunicationGroup
from .models import CommunicationGroupMembers


# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_communication = Blueprint('communication', __name__, url_prefix='/communication')

@mod_communication.context_processor
def props():
    props_dict = { 'serviceName': 'Communication',
                   'serviceDashboardUrl': url_for('communication.dashboard'),
                   'serviceBrowseUrl': url_for('communication.browse'),
                   'serviceNewUrl': url_for('communication.new'),
                 }
    return props_dict

# Set the route and accepted methods
@mod_communication.route('/', methods=['GET'])
def communication():
    return render_template('communication/communication_dashboard.html')


@mod_communication.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('communication/communication_dashboard.html')


@mod_communication.route('/browse', methods=['GET'])
def browse():
    return render_template('communication/communication_browse.html')


@mod_communication.route('/new', methods=['GET', 'POST'])
def new():
    return render_template('communication/communication_new.html')


@mod_communication.route('/profile', methods=['GET', 'POST'])
def profile():
    return render_template('communication/communication_profile.html')


@mod_communication.route('/view', methods=['GET', 'POST'])
def communication_view():
    return render_template('communication/communication_view.html')
